package com.communication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages="com")
public class CommunicationMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommunicationMicroserviceApplication.class, args);
	}

}

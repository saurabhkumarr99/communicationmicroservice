package com.service;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailServices {

	public static String sendOTP(String sendTo) {

		String OTP = String.valueOf((int) (Math.random() * 100000));
		String message = "OTP: " + OTP;
		String failed = "9999999";

		String subject = "Verify Your Email";

		 boolean status = sendEmail(sendTo, subject, message);
        //boolean status = sendOutlookEmail(sendTo, subject, message);
		//boolean status =sendfromOutlookEmail(sendTo, subject, message);
		// boolean status = true;

		if (status) {
			return OTP;
		} else {
			return "failed";
		}

	}

	public static boolean sendEmail(String sendTo, String subject, String message) {

		// Senders email
		final String from = "krishisathitest@gmail.com";

		// Variable for gmail
		String host = "smtp.gmail.com";

		// get the system properties
		Properties properties = System.getProperties();
		// setting important information to properties object

		// host set
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", "465");
		properties.put("mail.smtp.ssl.enable", "true");
		properties.put("mail.smtp.auth", "true");

		// Step 1: to get the session object..
		Session session = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, "xshgnetudehhfsyt");
			}
		});

		session.setDebug(true);

		// Step 2 : compose the message [text,multimedia]
		MimeMessage m = new MimeMessage(session);

		try {

			// from email
			m.setFrom(from);

			// adding recipient to message
			m.addRecipient(Message.RecipientType.TO, new InternetAddress(sendTo));

			// adding subject to message
			m.setSubject(subject);

			// adding text to message
			m.setText(message);

			// send

			// Step 3 : send the message using Transport class
			Transport.send(m);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public static boolean sendOutlookEmail(String sendTo, String subject, String message) {
		// Senders email (Gmail account)
		final String from = "krishisathitest@gmail.com"; // Replace with your Gmail email address
		final String password = "xshgnetudehhfsyt"; // Replace with your Gmail password or an app-specific password

		// Gmail SMTP server settings
		String host = "smtp.gmail.com";

		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");

		Session session = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, password);
			}
		});

		try {
			MimeMessage m = new MimeMessage(session);
			m.setFrom(new InternetAddress(from));
			m.addRecipient(Message.RecipientType.TO, new InternetAddress(sendTo));
			m.setSubject(subject);
			m.setText(message);

			Transport.send(m);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}
	
	 public static boolean sendfromOutlookEmail(String sendTo, String subject, String message) {
	        // Configuration for your Outlook email service
	        final String SMTP_HOST = "smtp.office365.com"; // Use "smtp.office365.com" for Office 365 accounts
	        final String SMTP_PORT = "587"; // Use port 587 for TLS

	        Properties properties = new Properties();
	        properties.put("mail.smtp.host", SMTP_HOST);
	        properties.put("mail.smtp.port", SMTP_PORT);
	        properties.put("mail.smtp.auth", "true");
	        properties.put("mail.smtp.starttls.enable", "true");

	        // Your Outlook email address and password
	        final String SMTP_USERNAME = "saurabh.rai@npci.org.in"; // Replace with your Outlook email address
	        final String SMTP_PASSWORD = "Octobe@202399"; // Replace with your Outlook password

	        Session session = Session.getInstance(properties, new Authenticator() {
	            @Override
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(SMTP_USERNAME, SMTP_PASSWORD);
	            }
	        });

	        try {
	            MimeMessage m = new MimeMessage(session);
	            m.setFrom(new InternetAddress(SMTP_USERNAME));
	            m.addRecipient(Message.RecipientType.TO, new InternetAddress(sendTo));
	            m.setSubject(subject);
	            m.setText(message);

	            Transport.send(m);
	            return true;
	        } catch (MessagingException e) {
	            e.printStackTrace();
	        }

	        return false;
	    }
}

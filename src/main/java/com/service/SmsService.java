package com.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SmsService {

	public static String sendOTP(String sendTo) {

		String OTP = String.valueOf((int) (Math.random() * 100000));
		String message = "OTP: " + OTP;
		String failed = "9999999";


		 boolean status = sendMessage(sendTo, message);
  

		if (status) {
			return OTP;
		} else {
			return "failed";
		}

	}

	public static boolean sendMessage(String sendTo, String message) {
	
	        // Your Twilio Account SID and Auth Token
	        String accountSid = "AC0bfc6fdc9c60cf75f4f2eb52b9d3ee8a";
	        String authToken = "6624f533658e5ea7998b611c0476ce10";

	        // Initialize Twilio
	        Twilio.init(accountSid, authToken);

	        // Recipient's phone number (in E.164 format, e.g., +1234567890)
	        String toPhoneNumber ="+91"+ sendTo;


	        try {
	            // Send the OTP via SMS
	            Message.creator(
	                new PhoneNumber(toPhoneNumber),
	                new PhoneNumber("+917905734964"), // Replace with your Twilio phone number
	                message
	            ).create();
	            return true; // OTP sent successfully
	        } catch (Exception e) {
	            e.printStackTrace();
	            return false; // Failed to send OTP
	        }
	}
	
}

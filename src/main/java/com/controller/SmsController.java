package com.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.SmsService;

@RestController
public class SmsController {

	@RequestMapping("/sendSms/{num}")
	public String sendOtp(@PathVariable("num") String sendTo) {
		return SmsService.sendOTP(sendTo);
	}
}

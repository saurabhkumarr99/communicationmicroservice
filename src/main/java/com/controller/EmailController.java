package com.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.EmailServices;

@RestController
public class EmailController {

	@RequestMapping("/sendOtp/{email}")
	public String sendOtp(@PathVariable("email") String sendTo) {
		return EmailServices.sendOTP(sendTo);
	}
}
